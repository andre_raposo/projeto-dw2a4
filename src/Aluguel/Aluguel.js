import {
    v4 as uuidv4
} from 'uuid';

export default class Aluguel {
    constructor(usuarioId, bicicletaId, id = uuidv4()) {
        this.id = id
        this.usuarioId = usuarioId
        this.bicicletaId = bicicletaId
        this.AlugadoEm = new Date().toISOString()
    }
}