import express from 'express'
import AluguelController from './AluguelController'
import AluguelRepository from './AluguelRepository'
import {
    client
} from '../config'

export default function BicicletaRouter() {

    const aluguelRepository = new AluguelRepository(client)
    const aluguelController = new AluguelController(aluguelRepository)

    const router = express.Router()

    router.route('/novo')
        .post((req, res) => aluguelController.save(req, res))

    router.route('/:id')
        .get((req, res) => aluguelController.show(req, res))

    return router
}