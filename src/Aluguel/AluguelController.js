import Aluguel from './Aluguel'

const aluguelReturn = aluguel => ({
    id: aluguel.id,
    usuarioId: aluguel.usuarioId,
    bicicletaId: aluguel.bicicletaId
})

export default class AluguelController {
    constructor(AluguelRepository) {
        this.AluguelRepository = AluguelRepository
    }

    async save(req, res) {
        const usuarioId = req.body.usuarioId
        const bicicletaId = req.body.bicicletaId
        const aluguel = new Aluguel(usuarioId, bicicletaId)
        await this.AluguelRepository.save(aluguel)
        return res.status(201).json(aluguelReturn(aluguel))
    }

    async show(req, res) {
        const aluguel = await this.AluguelRepository.getById(req.params.id)
        console.log("TESTE")
        console.log(aluguel)
        if (!aluguel) {
            return res.status(404).json({
                error: 'id não encontrado'
            })
        }
        return res.status(200).json(aluguelReturn(aluguel))
    }

}