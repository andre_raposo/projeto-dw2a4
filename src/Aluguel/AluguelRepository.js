import {
    client
} from '../config'

export default class AluguelRepository {
    constructor() {
        this.client = client;

    }
    async getById(id) {
        try {
            return await this.client('alugueis')
                .where({
                    id: id
                })
                .first()
        } catch (err) {
            console.log(err)
        }
    }

    async save(aluguel) {
        try {
            const [first_row] = await this.client('alugueis')
                .insert(aluguel)
                .returning('*')
            return [first_row]
        } catch (err) {
            console.log(err)
        }
    }
}