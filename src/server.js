import MyApp from './app'

const app = new MyApp();

app.listen(3000, () => {
    console.log('Servidor rodando na porta 3000!');
});