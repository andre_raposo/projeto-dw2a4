import express from 'express';
import cors from 'cors';

import UsuarioRouter from './Usuario/UsuarioRouter'
import BicicletaRouter from './Bicicleta/BicicletaRouter'
import AluguelRouter from './Aluguel/AluguelRouter'

export default function MyApp() {
    const app = express()

    app.use(cors())
    app.use(express.json())

    app.get('/', function(req, res) {
        res.send('Servidor rodando...')
    })

    app.use('/usuario', UsuarioRouter())
    app.use('/bicicleta', BicicletaRouter())
    app.use('/aluguel', AluguelRouter())

    return app;
}