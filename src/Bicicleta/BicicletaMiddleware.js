export default class BicicletaMiddleware {
    constructor(BicicletaRepository) {
        this.BicicletaRepository = BicicletaRepository
    }

    async bicicletaExiste(req, res, next) {
        const bicicleta = await this.BicicletaRepository.getById(req.params.id)
        if (!bicicleta) {
            return res.status(404).json({
                erro: 'Bicicleta não encontrada'
            });
        }
        req.bicicleta = bicicleta;
        next();
    }
}