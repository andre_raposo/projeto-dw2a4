export default class BicicletaRepository {

    constructor(client) {
        this.client = client;
    }

    async getAll() {
        return await this.client('bicicletas');
    }

    async save(bicicleta) {
        try {
            const [firstRow] = await this.client('bicicletas')
                .insert(bicicleta)
                .returning("*")
            return firstRow;
        }
        catch (err) {
            console.log(err)
        }
    }

    async getById(id) {
        try {
            return await this.client('bicicletas')
                .where({
                    id: id
                })
                .first()
        }
        catch (err) {
            console.log(err)
        }
    }

    async update(bicicleta) {
        try {
            const [firstRow] = await this.client('bicicletas')
                .where({
                    id: bicicleta.id
                })
                .update({
                    modelo: bicicleta.modelo,
                    tipo: bicicleta.tipo,
                    status: bicicleta.status,
                })
                .returning("*")
            return firstRow;
        }
        catch (err) {
            console.log(err)
        }   
    }

    async delete(bicicleta) {
        try {
            await this.client('bicicletas')
                .where({
                    id: bicicleta.id
                })
                .del()
        }
        catch (err) {
            console.log(err)
        }    
    }
}