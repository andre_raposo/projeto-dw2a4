import express from 'express'
import BicicletaController from './BicicletaController'
import BicicletaMiddleware from './BicicletaMiddleware'
import BicicletaRepository from './BicicletaRepository'
import { client } from '../config'

export default function BicicletaRouter() {

    const bicicletaRepository = new BicicletaRepository(client)
    const bicicletaController = new BicicletaController(bicicletaRepository)
    const bicicletaMiddleware = new BicicletaMiddleware(bicicletaRepository)

    const router = express.Router()

    router.route('/cadastro')
        .post((req, res) => bicicletaController.save(req, res))

    router.route('/:id')
        .all((req, res, next) => bicicletaMiddleware.bicicletaExiste(req, res, next))
        .get((req, res) => bicicletaController.show(req, res))
        .put((req, res) => bicicletaController.update(req, res))
        .delete((req, res) => bicicletaController.delete(req, res))

    return router
}