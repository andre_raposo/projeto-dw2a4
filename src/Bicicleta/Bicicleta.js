import { v4 as uuidv4 } from 'uuid';

export default class Bicicleta {
  constructor(id = uuidv4(), modelo, tipo, status) {
    this.id = id
    this.modelo = modelo;
    this.tipo = tipo;
    this.status = status;
  }
}
