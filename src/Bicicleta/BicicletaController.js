import Bicicleta from './Bicicleta';

const bicicletaReturn = bicicleta => ({
    id: bicicleta.id,
    modelo: bicicleta.modelo,
    tipo: bicicleta.tipo,
    status: bicicleta.status
})

export default class BicicletaController {
    constructor(BicicletaRepository) {
        this.bicicletaRepository = BicicletaRepository
    }

    async save(req, res) {
        const { id, modelo, tipo, status } = req.body
        const bicicleta = new Bicicleta(id, modelo, tipo, status)
        await this.bicicletaRepository.save(bicicleta)
        res.status(201).json(bicicletaReturn(bicicleta))
    }

    show(req, res) {
        return res.status(200).json(bicicletaReturn(req.bicicleta))
    }

    async update(req, res) {
        const { id, modelo, tipo, status } = req.body
        const bicicleta = new Bicicleta(req.bicicleta.id, modelo, tipo, status);
        const bicicletaAtualizada = await this.bicicletaRepository.update(bicicleta)
        res.status(200).json(bicicletaReturn(bicicletaAtualizada))
    }

    async delete(req, res) {
        await this.bicicletaRepository.delete(req.bicicleta)
        res.status(204).end()
    }
}
