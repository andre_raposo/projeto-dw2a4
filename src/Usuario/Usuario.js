import {
    v4 as uuidv4
} from 'uuid';
import sha256 from 'crypto-js/sha256';

export default class Usuario {

    constructor(nome, email, senha, active = false, id = uuidv4()) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.senha = sha256(senha).toString()
        this.active = active
        this.criadoEm = new Date().toISOString();
        this.atualizadoEm = new Date().toISOString();
        this.deletadoEm = null;
    }
}