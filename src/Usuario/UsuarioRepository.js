export default class UsuarioRepository {

    constructor(client) {
        this.client = client;
    }

    async getAll() {
        return await this.client('usuarios');
    }

    async getById(id) {
        try {
            return await this.client('usuarios')
                .where({
                    id: id
                })
                .first()
        } catch (err) {
            console.log(err)
        }
    }

    async getByEmail(email) {
        try {
            return await this.client('usuarios')
                .where({
                    email: email
                })
                .first()
        } catch (err) {
            console.log(err)
        }
    }

    async save(usuario) {
        try {
            const [firstRow] = await this.client('usuarios')
                .insert(usuario)
                .returning("*")
            return firstRow
        } catch (err) {
            console.log(err)
        }
    }

    async update(usuario) {
        try {
            const [firstRow] = await this.client('usuarios')
                .where({
                    id: usuario.id
                })
                .update({
                    nome: usuario.nome,
                    email: usuario.email,
                    senha: usuario.senha,
                    active: usuario.active,
                    atualizadoEm: new Date().toISOString(),
                })
                .returning('*')
            return firstRow
        } catch (err) {
            console.log(err)
        }
    }

    async delete(usuario) {
        try {
            await this.client('usuarios')
                .where({
                    id: usuario.id
                })
                .del()
        } catch (err) {
            console.log(err)
        }
    }
}