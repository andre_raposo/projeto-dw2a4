import Usuario from './Usuario';
import UsuarioMiddleware from './UsuarioMiddleware'

const usuarioReturn = usuario => ({
    id: usuario.id,
    nome: usuario.nome,
    email: usuario.email,
})

export default class UsuarioController {

    constructor(UsuarioRepository) {
        this.usuarioRepository = UsuarioRepository;
    }

    async getAll(req, res) {
        const usuarios = await this.usuarioRepository.getAll('usuarios')
        res.status(200).json(usuarios.map(usuario => usuarioReturn(usuario)))
    }

    async save(req, res) {
        const {
            nome,
            email,
            senha
        } = req.body
        const usuario = new Usuario(nome, email, senha)
        await this.usuarioRepository.save(usuario)
        res.status(201).json(usuarioReturn(usuario))
    }

    show(req, res) {
        return res.status(200).json(usuarioReturn(req.usuario))
    }

    async update(req, res) {
        const {
            nome,
            email,
            senha
        } = req.body
        const usuario = new Usuario(nome, email, senha, false, req.usuario.id);
        const usuarioAtualizado = await this.usuarioRepository.update(usuario)
        res.status(200).json(usuarioReturn(usuarioAtualizado))
    }

    async delete(req, res) {
        await this.usuarioRepository.delete(req.usuario)
        res.status(204).end()
    }

    async login(req, res) {
        const usuarioMiddleware = new UsuarioMiddleware(this.usuarioRepository);
        const usuario = await usuarioMiddleware.emailExiste(req.body.email, res)

        if (usuarioMiddleware.validaSenha(req.body.senha, usuario)) {
            try {
                const usuarioAtualizado = new Usuario(usuario.nome, usuario.email, usuario.senha,
                    true, usuario.id)
                await this.usuarioRepository.update(usuarioAtualizado)
                res.status(200).json({
                    success: 'Login realizado'
                })
            } catch (err) {
                console.log(err)
            }
        } else {
            res.status(400).json({
                erro: "Senha inválida"
            })
        }
    }

    async logout(req, res) {
        const usuarioMiddleware = new UsuarioMiddleware(this.usuarioRepository)
        const usuario = await usuarioMiddleware.emailExiste(req.body.email, res)
        const usuarioAtualizado = new Usuario(usuario.nome, usuario.email, usuario.senha,
            true, usuario.id)

        try {
            await this.usuarioRepository.update(usuarioAtualizado)
            res.status(200).json({
                success: "Logout realizado com sucesso"
            })
        } catch (err) {
            console.log(err)
        }
    }
}