import sha256 from 'crypto-js/sha256'

export default class UsuarioMiddleware {
    constructor(UsuarioRepository) {
        this.UsuarioRepository = UsuarioRepository
    }

    async usuarioExiste(req, res, next) {
        const usuario = await this.UsuarioRepository.getById(req.params.id)

        if (!usuario) {
            return res.status(404).json({
                erro: "Usuario não encontrado"
            })
        }

        req.usuario = usuario
        next()
    }

    async emailExiste(email, res) {
        try {
            const usuario = await this.UsuarioRepository.getByEmail(email)
            if (!usuario)
                return res.status(404).json({
                    erro: "Email não encontrado"
                })
            return usuario

        } catch (err) {
            console.log(err)
        }
    }

    validaSenha(senha, usuario) {
        const senha_body = sha256(senha).toString()
        if (senha_body == usuario.senha) return true
        else return false
    }
}